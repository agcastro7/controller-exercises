Rails.application.routes.draw do
  resources :trips
  put '/book/:id', to: 'trips#book_seat', as: :book_seat
  put '/unbook/:id', to: 'trips#unbook_seat', as: :unbook_seat
  put '/clear/:id', to: 'trips#clear', as: :clear_trip
  get '/find-a-trip', to: 'trips#find', as: :find_trip
  post '/search', to: 'trips#search', as: :search_trip
  get '/destinations', to: 'trips#destinations', as: :destinations
  root to: 'trips#index'
end
