# == Schema Information
#
# Table name: trips
#
#  id           :integer          not null, primary key
#  price        :float
#  origin       :string
#  destination  :string
#  seats        :integer
#  booked_seats :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryBot.define do
  factory :trip do
    price { 1.5 }
    origin { "MyString" }
    destination { "MyString" }
    seats { 1 }
    booked_seats { 1 }
  end
end
