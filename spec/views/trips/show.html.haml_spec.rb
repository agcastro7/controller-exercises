require 'rails_helper'

RSpec.describe "trips/show", type: :view do
  before(:each) do
    @trip = assign(:trip, Trip.create!(
      :price => 2.5,
      :origin => "Origin",
      :destination => "Destination",
      :seats => 3,
      :booked_seats => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2.5/)
    expect(rendered).to match(/Origin/)
    expect(rendered).to match(/Destination/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
  end
end
