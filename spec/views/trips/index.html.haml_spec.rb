require 'rails_helper'

RSpec.describe "trips/index", type: :view do
  before(:each) do
    assign(:trips, [
      Trip.create!(
        :price => 2.5,
        :origin => "Origin",
        :destination => "Destination",
        :seats => 3,
        :booked_seats => 4
      ),
      Trip.create!(
        :price => 2.5,
        :origin => "Origin",
        :destination => "Destination",
        :seats => 3,
        :booked_seats => 4
      )
    ])
  end

  it "renders a list of trips" do
    render
    assert_select "tr>td", :text => 2.5.to_s, :count => 2
    assert_select "tr>td", :text => "Origin".to_s, :count => 2
    assert_select "tr>td", :text => "Destination".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
