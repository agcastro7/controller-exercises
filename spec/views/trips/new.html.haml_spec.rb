require 'rails_helper'

RSpec.describe "trips/new", type: :view do
  before(:each) do
    assign(:trip, Trip.new(
      :price => 1.5,
      :origin => "MyString",
      :destination => "MyString",
      :seats => 1,
      :booked_seats => 1
    ))
  end

  it "renders new trip form" do
    render

    assert_select "form[action=?][method=?]", trips_path, "post" do

      assert_select "input[name=?]", "trip[price]"

      assert_select "input[name=?]", "trip[origin]"

      assert_select "input[name=?]", "trip[destination]"

      assert_select "input[name=?]", "trip[seats]"

      assert_select "input[name=?]", "trip[booked_seats]"
    end
  end
end
