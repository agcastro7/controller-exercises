class CreateTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.float :price
      t.string :origin
      t.string :destination
      t.integer :seats
      t.integer :booked_seats

      t.timestamps
    end
  end
end
