class ChangeTripBookedSeatsDefaultToZero < ActiveRecord::Migration[5.2]
  def change
    change_column_default(
      :trips,
      :booked_seats,
      from: nil,
      to: 0
    )
  end
end
