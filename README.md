# Controller exercises

To download this code, execute in bash:

```bash
git clone git@bitbucket.org:agcastro7/controller-exercises.git
cd controller-exercises
bundle
rails db:migrate
rails s
```
