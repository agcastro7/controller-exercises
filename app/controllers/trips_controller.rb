class TripsController < ApplicationController

  def index
    @trips = nil
  end

  def show
    @trip = nil
  end

  def new
    @trip = nil
  end

  def edit
    @trip = nil
  end

  def create
    @trip = nil
    @trip.save
  end

  def update
    @trip = nil
    @trip.update(params[:trip])
  end

  def destroy
    @trip = nil
    @trip.destroy
  end

  def book_seat
  end

  def unbook_seat
  end

  def clear
  end

  def find
  end

  def search
  end

  def destinations
    @destinations = Trip.distinct.pluck(:destination)
  end
end
