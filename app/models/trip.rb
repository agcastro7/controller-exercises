# == Schema Information
#
# Table name: trips
#
#  id           :integer          not null, primary key
#  price        :float
#  origin       :string
#  destination  :string
#  seats        :integer
#  booked_seats :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Trip < ApplicationRecord

  validates :price, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :origin, presence: true
  validates :destination, presence: true
  validates :seats, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :booked_seats, numericality: { greater_than_or_equal_to: 0 }

  def full?
    seats == booked_seats
  end

  def available_seats
    seats - booked_seats
  end

  def price_formatted
    "#{sprintf('%.2f', price)} €"
  end

  def book
    self.booked_seats = self.booked_seats < self.seats ? self.booked_seats + 1 : self.booked_seats
  end

  def unbook
    self.booked_seats = self.booked_seats > 0 ? self.booked_seats - 1 : self.booked_seats
  end
end
