module TripsHelper
  def origins_for_select
    options_for_select(Trip.distinct.pluck(:origin))
  end

  def destinations_for_select
    options_for_select(Trip.distinct.pluck(:destination))
  end
end
